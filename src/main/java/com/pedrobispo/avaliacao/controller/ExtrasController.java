package com.pedrobispo.avaliacao.controller;

import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

import com.pedrobispo.avaliacao.model.Divisao;

/**
* Controller da tela de extras.
*
* @author  Pedro Bispo
*/
@Named
@ViewScoped
public class ExtrasController{
    
    private Divisao divisao;

    /**
     * Recebe o valor do primeiro número para gerar a sequência numérica.
     */
    private int numeroInicial;

    /**
    * Recebe o valor do segundo número para gerar a sequência numérica.
    */
    private int numeroFinal;

    /**
    * Contém a sequência de números iniciando com o valor do numeroInicial indo até o valor do numeroFinal.
    */
    private String sequenciaGerada;

    /**
     * Recebe os elementos da sequência numérica gerada entre numeroInicial
     * e numeroFinal
     */
    private ArrayList<Integer> arraySequencia;

    

    /**
    * Seta os valores iniciais das variáveis.
    */
    @PostConstruct
    private void init(){
        numeroInicial = 0;
        numeroFinal = 0;
        divisao = new Divisao();
        divisao.setQuociente("0");
        sequenciaGerada = "";
    }

    /**
     * Executa o algorítimo que gera a sequência de números entre os valores
     * informados nas variáveis numeroInicial e numeroFinal.
     * 
     * Caso os valores sejam iguais, retorna o valor da variável numeroInicial,
     * acaso contrário, executa o for correspondente, adicionando cada número
     * na variável arraySequencia e chamando o método formatarSequencia.
     * 
     * @see formatarSequencia
     */
    public void gerarSequencia(){
        sequenciaGerada = "";   //limpa o campo de texto
        
        if(numeroInicial == numeroFinal){   //caso os valores sejam iguais, exibe o própio valor.
            sequenciaGerada = Integer.toString(numeroInicial);
        }else{
            arraySequencia = new ArrayList<>();
            
            if(numeroInicial < numeroFinal){    

                for(int i = numeroInicial; numeroFinal >= i; i++){ // se o número inicial for menor que o número
                                                                     // final, a sequência é incrementada até
                                                                     // numeroFinal >= i
                    arraySequencia.add(i);
                }

                formatarSequencia();

            }else{ 

                for(int i = numeroInicial; numeroFinal <= i; i--){  // se o número inicial for maior que o número
                                                                     // final, a sequência é decrementada até
                                                                     // numeroFinal <= i
                    arraySequencia.add(i);
                }

                formatarSequencia();
            }

        }
        
    }

    /**
     * formata a string a ser enviada à caixa de texto, removendo os colchetes do início e do fim do toString no objeto @{arraySequencia}.
     */
    private void formatarSequencia(){
        sequenciaGerada = arraySequencia.toString();
        sequenciaGerada = sequenciaGerada.substring(1, sequenciaGerada.length()-1);
    }

    /**
    * reseta os valores dos campos numéricos para nova operação.
    */
    public void limparCampos(){
        divisao.setDivisor(0);
        divisao.setResto(0);
        divisao.setQuociente(Integer.toString(0));
    }

    public int getNumeroInicial() {
        return numeroInicial;
    }

    public void setNumeroInicial(int numeroInicial) {
        this.numeroInicial = numeroInicial;
    }

    public int getNumeroFinal() {
        return numeroFinal;
    }

    public void setNumeroFinal(int numeroFinal) {
        this.numeroFinal = numeroFinal;
    }

    public String getSequenciaGerada() {
        return sequenciaGerada;
    }

    public void setSequenciaGerada(String sequenciaGerada) {
        this.sequenciaGerada = sequenciaGerada;
    }

    public Divisao getDivisao() {
        return divisao;
    }

    public void setDivisao(Divisao divisao) {
        this.divisao = divisao;
    }

    /**
     * @return ArrayList<Integer> return the arraySequencia
     */
    public ArrayList<Integer> getArraySequencia() {
        return arraySequencia;
    }

    /**
     * @param arraySequencia the arraySequencia to set
     */
    public void setArraySequencia(ArrayList<Integer> arraySequencia) {
        this.arraySequencia = arraySequencia;
    }

}