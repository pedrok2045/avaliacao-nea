package com.pedrobispo.avaliacao.controller;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import com.pedrobispo.avaliacao.model.Contribuinte;
import com.pedrobispo.avaliacao.model.Rua;
import com.pedrobispo.avaliacao.repository.ContribuinteRepository;
import com.pedrobispo.avaliacao.repository.RuaRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
* Controller da tela de cadastro de contribuintes.
*
* @author  Pedro Bispo
*/
@Named
@ViewScoped
public class ContribuinteControllerCadastro{
    
    /**
     * Injeção de dependência para manipular o objeto Contribuinte no banco de dados.
     */
    @Autowired
    private ContribuinteRepository contribuinteRepository;

    /**
     * Injeção de dependência para manipular o objeto Rua no banco de dados.
     */
    @Autowired
    private RuaRepository ruaRepository;

    /**
     * Lista de ruas para popular o componente de seleção de ruas.
     */
    private List<Rua> ruas;

    /**
     * Contribuinte a ser persistido no banco de dados e ao qual o formulário faz referência.
     */
    private Contribuinte contribuinte;

    /**
     * Busca as ruas cadastradas no banco de dados e limpa a variável contribuinte.
     */
    @PostConstruct
    private void init(){
        ruas = ruaRepository.findAll();
        contribuinte = new Contribuinte();
    }

    /**
     * Salva o contribuinte no banco de dados e limpa os campos do formulário.
     */
    public void salvar(){
        contribuinteRepository.save(contribuinte);
        contribuinte = new Contribuinte();
    }
    
    public Contribuinte getContribuinte() {
        return contribuinte;
    }

    public void setContribuinte(Contribuinte contribuinte) {
        this.contribuinte = contribuinte;
    }


    public List<Rua> getRuas() {
        return ruas;
    }

    public void setRuas(List<Rua> ruas) {
        this.ruas = ruas;
    }

}