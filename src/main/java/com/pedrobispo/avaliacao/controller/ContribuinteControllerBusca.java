package com.pedrobispo.avaliacao.controller;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import com.pedrobispo.avaliacao.model.Contribuinte;
import com.pedrobispo.avaliacao.repository.ContribuinteRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
* Controller da tela de buscas dos contribuintes.
* 
* @author  Pedro Bispo
*/
@Named
@ViewScoped
public class ContribuinteControllerBusca{
    

    /**
     * Injeção de dependência para manipular o objeto Contribuinte no banco de
     */
    @Autowired
    private ContribuinteRepository contribuinteRepository;

    /**
     * Contribuintes buscados no banco de dados.
     */
    private List<Contribuinte> contribuintes;

    /**
     * Recebe o valor que indica o tipo de busca que o usuário quer realizar,
     * valor proveniente dos radioButtons na tela.
     */
    private String tipoDeBusca;

    /**
     * Recebe o termo digitado na caixa de busca.
     */
    private String termo;

    /**
     * configura os valores iniciais dos campos de texto.
     */
    @PostConstruct
    private void init(){
        this.tipoDeBusca = "";
        this.termo = "";
    }

    /**
     * De acordo com o tipo de busca (valor da variável 'tipo de busca'),
     * chama o método correspondente.
     */
    public void buscarContribuintes(){
        switch(tipoDeBusca){
            case "inicia":
            buscaPorTermoNoInicio();
            break;

            case "possui":
            buscaPorPossuirTermo();
            break;

            case "termina":
            buscaPorTermoNoFim();
            break;

            case "igual":
            buscaPorTermoIgual();
            break;
        }
    }

    /**
     * Método que busca o contribuinte cujo nome começa com o termo informado no
     * campo de busca.
     */
    public void buscaPorTermoNoInicio(){
        contribuintes = contribuinteRepository.nomeIniciaCom(termo);
        limparCampoDeBusca();
    }

    /**
     * Método que busca o contribuinte cujo nome possui o termo informado no campo
     * de busca.
     */
    public void buscaPorPossuirTermo() {
        contribuintes = contribuinteRepository.nomePossui(termo);
        limparCampoDeBusca();
    }

    /**
     * Método que busca o contribuinte cujo nome termina com o termo informado no campo de
     * busca.
     */
    public void buscaPorTermoNoFim(){
        contribuintes = contribuinteRepository.nomeTerminaCom(termo);
        limparCampoDeBusca();
    }

    /**
     * Método que busca o contribuinte que tem o nome igual ao termo informado no campo de
     * busca.
     */
    public void buscaPorTermoIgual(){
        contribuintes = contribuinteRepository.nomeIgualAh(termo);
        limparCampoDeBusca();
    }

    /**
     * Limpa o valor do campo de busca somente se a busca retornar algum valor.
     */
    private void limparCampoDeBusca(){
        if(contribuintes.size() > 0){
            termo = "";
        }
    }

    public ContribuinteRepository getContribuinteRepository() {
        return contribuinteRepository;
    }

    public void setContribuinteRepository(ContribuinteRepository contribuinteRepository) {
        this.contribuinteRepository = contribuinteRepository;
    }

    public List<Contribuinte> getContribuintes() {
        return contribuintes;
    }

    public void setContribuintes(List<Contribuinte> contribuintes) {
        this.contribuintes = contribuintes;
    }

    public String getTipoDeBusca() {
        return tipoDeBusca;
    }

    /**
     * Recebe o valor dos radioButtons na tela, seta na variável tipoDeBusca e limpa o campo de busca.
     * 
     * @param tipoDeBusca valor passado pelos radioButtons para setar na variável tipoDeBusca
     */
    public void setTipoDeBusca(String tipoDeBusca) {
        this.tipoDeBusca = tipoDeBusca;
        limparCampoDeBusca();
    }

    public String getTermo() {
        return termo;
    }

    public void setTermo(String termo) {
        this.termo = termo;
    }




}