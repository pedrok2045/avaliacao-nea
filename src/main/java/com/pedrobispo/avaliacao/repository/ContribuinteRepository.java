package com.pedrobispo.avaliacao.repository;

import java.util.List;
import com.pedrobispo.avaliacao.model.Contribuinte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
* Responsável pela comunicação com o banco de dados referente ao objeto do tipo Contribuinte.
*
* @author  Pedro Bispo
*/
@Repository
public interface ContribuinteRepository extends JpaRepository<Contribuinte, Integer>{

    /**
     * Busca um controbuinte no sistema pelo campo nome.
     * 
     * @param nome valor do campo nome a ser buscado no banco de dados.
     * @return Contribuinte encontrado
     */
    @Query("Select contribuinte from Contribuinte contribuinte where contribuinte.nome =:nome")
	Contribuinte findByName(@Param("nome") String nome);

    /**
     * Busca todos os contribuintes cujo valor do campo nome inicie com o termo passado como argumento.
     * 
     * @param termo texto inicial que o atributo nome deve conter.
     * @return Todos os contribuintes cujo valor do atributo nome começe com o termo informado.
     */
    @Query(value = "SELECT contribuinte FROM Contribuinte contribuinte join contribuinte.rua WHERE contribuinte.nome like :termo%", nativeQuery = false)
    List<Contribuinte> nomeIniciaCom(@Param("termo") String termo);

    /**
     * Busca todos os contribuintes cujo valor do campo nome possua o termo passado como argumento.
     * 
     * @param termo texto que o atributo nome deve conter em qualquer parte do seu valor.
     * @return Todos os contribuintes em que o valor do atributo nome contenha o termo informado.
     */
    @Query(value = "SELECT contribuinte FROM Contribuinte contribuinte join contribuinte.rua WHERE contribuinte.nome like %:termo%", nativeQuery = false)
	List<Contribuinte> nomePossui(@Param("termo") String termo);

    /**
     * Busca todos os contribuintes cujo valor do campo nome termine com o termo passado como argumento.
     * 
     * @param termo texto que o atributo nome deve conter no fim do seu valor.
     * @return Todos os contribuintes em que o valor do atributo nome termine com o termo informado.
     */
    @Query(value = "SELECT contribuinte FROM Contribuinte contribuinte join contribuinte.rua WHERE contribuinte.nome like %:termo", nativeQuery = false)
	List<Contribuinte> nomeTerminaCom(@Param("termo") String termo);

    /**
     * Busca todos os contribuintes cujo valor do campo nome seja igual ao termo passado como argumento.
     * 
     * @param termo texto ao qual o valor do campo nome deva ser igual.
     * @return Todos os contribuintes em que o valor do atributo nome seja igual ao termo informado.
     */
    @Query(value = "SELECT contribuinte FROM Contribuinte contribuinte join contribuinte.rua WHERE contribuinte.nome like :termo", nativeQuery = false)
	List<Contribuinte> nomeIgualAh(@Param("termo") String termo);

}