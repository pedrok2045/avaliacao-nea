package com.pedrobispo.avaliacao.repository;

import com.pedrobispo.avaliacao.model.Rua;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
* Responsável pela comunicação com o banco de dados referente ao objeto do tipo Rua.
*
* @author  Pedro Bispo
*/
@Repository
public interface RuaRepository extends JpaRepository<Rua, Integer>{

}