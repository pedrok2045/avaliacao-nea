package com.pedrobispo.avaliacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;


/**
* <h1>Avaliação</h1>
* Sistema desenvolvido para avaliação da empresa N & A Informática.
* <p>
* Consiste em 3 telas, onde na primeira deve-se cadastrar um contribuinte,
* na segunda fazer uma busca no banco de dados por esses contribuintes
* e na terceira tela, montar uma interface para que o usuário possa
* informar dois números e o sistema mostrar todos os números no intervalo entre eles
* e também que o usuário possa informar dois números e o sistema realizar a divisão
* entre eles, de modo que o algorítmo não utilize a operação de divisão.
* <p><p>
*
* @author  Pedro Bispo
*/
@SpringBootApplication
@ComponentScan
public class AvaliacaoApplication extends SpringBootServletInitializer{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application){
		return application.sources(AvaliacaoApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(AvaliacaoApplication.class, args);
	}

}
