package com.pedrobispo.avaliacao.model;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * <h1>Representa uma rua no sistema.</h1>
 * <p>
 * Os nomes de referência dos atributos com os campos no banco de dados estão
 * entre '\' para que o postgres aceite os nomes em uppercase.
 *
 * @author Pedro Bispo
 */
@Entity
@Table(name = "\"RUA\"")
public class Rua implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "\"COD_RUA\"")
    private int id;

    @Column(name = "\"TIPO\"")
    private String tipo;

    @NotEmpty(message = "Informe o nome da rua")
    @Column(name = "\"NOME\"")
    private String nome;

    @Column(name = "\"DATA_CADASTRO\"", columnDefinition = "DATE")
    private Date data_cadastro;

    public Rua addTipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    public Rua addNome(String nome) {
        this.nome = nome;
        return this;
    }

    public Rua addData_cadastro(Date data_cadastro) {
        this.data_cadastro = data_cadastro;
        return this;
    }

    public int getId() {
        return id;
    }

    public String getTipo() {
        return tipo;
    }

    public String getNome() {
        return nome;
    }

    public Date getData_cadastro() {
        return data_cadastro;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setData_cadastro(Date data_cadastro) {
        this.data_cadastro = data_cadastro;
    }

    @Override
    public String toString() {
        return nome;
    }
    
}