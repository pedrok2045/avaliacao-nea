package com.pedrobispo.avaliacao.model;

/**
 * <h1>Classe que representa a operação de divisão</h1>
 * 
 * @author Pedro Bispo
 */
public class Divisao {

    /**
     * Valor do campo do primeiro número informado na divisão.
     */
    private int dividendo;

    /**
     * Valor do campo do segundo número informado na divisão.
     */
    private int divisor;

    /**
     * Contém o valor do resto da divisão entre os dois números informados.
     */
    private int resto;

    /**
     * Contém a parte inteira da divisão entre os dois números informados.
     * Essa variável só tem o seu tipo como String para poder exibir no
     * mesmo campo o resultado "Infinito" quando a divisão for por 0.
     */
    private String quociente;

    /**
     * Variável para guardar o valor do módulo do dividendo.
     */
    private int moduloDividendo;

    /**
     * Variável para guardar o valor do módulo do divisor.
     */
    private int moduloDivisor;

    /**
     * método que reseta os valores das variáveis principais
     * @return this
     */
    public Divisao reset(){
        this.setResto(0);
        this.setQuociente("0");
        this.addDividendo(0).addDivisor(0);
        return this;
    }

    /**
     * Chama o método que seta os valores dos módulos, em seguida inicia a análise
     * dos casos espefícicos e, caso passe na análise, isto é, não se enquadre em
     * nenhum dos casos específicos, executa a divisão.
     * <p>
     * @see setarModulos
     * @see precisaDividir
     * @see executarDivisao
     */
    public Divisao dividir() {

        setarModulos();

        if (precisaDividir()) {
            executarDivisao();
        }

        return this;
    }

    /**
     * <h1>Método que executa a divisão.</h1>
     * <p>
     * O resultado já começa com 1 pois o método será chamado somente quando o
     * dividendo for maior que o divisor, implicando que o resultado será sempre
     * maior que 1.
     */
    private void executarDivisao() {
        int resultadoParcial = 1;

        while ((moduloDividendo - moduloDivisor) >= moduloDivisor) { // executa as subtrações
            moduloDividendo = moduloDividendo - moduloDivisor; // atribui o valor da subtração à variável _dividendo.
            resultadoParcial++; // atualiza o resultado parcial.
        }

        quociente = Integer.toString(resultadoParcial * getSinal()); // seta o valor final da divisão.
        resto = getModulo(moduloDividendo - moduloDivisor); // seta o valor do resto
    }

    /**
     * <h1>Verifica se precisa dividir ou se é um caso espefífico com valor pré-determinado.</h1>
     * <p>
     * Uma vez que na divisão há certos casos específicos, este método analisa os
     * dados informados e verifica tais casos como divisão por zero, zero dividido
     * por qualquer número, um número dividido por ele mesmo e, como deve-se retornar
     * somente a parte inteira, verifica também se o divisor é maior que o
     * dividendo, o que resultaria em um valor menor do que 1.
     * <p>
     * @return false se os valores informados se encaixarem nos casos específicos e
     *         true caso nãos e encaixem.
     */
    private boolean precisaDividir() {

        if (dividendo == 0) { // 0 dividido por qualquer número é zero.
            quociente = Integer.toString(0);
            resto = 0;
            return false;
        } else if (divisor == 0) { // qualquer número dividido por zero tende ao infinito.
            quociente = "Infinito";
            return false;
        } else if (moduloDividendo < moduloDivisor) { // divisor maior que o dividendo é uma fração menor que 1, ou
                                                      // seja, resultado ZERO e resto igual ao dividendo.
            quociente = Integer.toString(0);
            resto = dividendo;
            return false;
        } else if (moduloDividendo == moduloDivisor) { // módulos iguais, resultado 1 com resto ZERO. Deve-se considerar
                                                       // o sinal, pois pode ser um resultado negativo.
            quociente = Integer.toString(1 * getSinal());
            resto = 0;
            return false;
        }
        return true;
    }

    /**
     * Analisa o sinal do resultado final da divisão, levando em conta que ++ ou --
     * é sempre + e qualquer outra possibilidade é sempre -
     * 
     * @return 1 para resultado positivo e -1 para resultado negativo.
     */
    private int getSinal() {
        if ((dividendo > 0 && divisor > 0) || (dividendo < 0 && divisor < 0)) {
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Seta as variáveis moduloDividendo e moduloDivisor com o valor dos módulos das
     * varáveis dividendo e divisor respectivamente.
     */
    private void setarModulos() {
        moduloDividendo = getModulo(dividendo);
        moduloDivisor = getModulo(divisor);
    }

    /**
     * Retorna o módulo de um número.
     * 
     * @param valor número do qual se quer obter o módulo.
     * @return módulo do número passado como parâmetro.
     */
    private int getModulo(int valor) {
        return valor < 0 ? valor * -1 : valor;
    }


    public int getResto() {
        return resto;
    }

    public void setResto(int resto) {
        this.resto = resto;
    }

    public String getQuociente() {
        return quociente;
    }

    public void setQuociente(String quociente) {
        this.quociente = quociente;
    }

    public int getDividendo() {
        return dividendo;
    }

    public void setDividendo(int dividendo) {
        this.dividendo = dividendo;
    }

    public int getDivisor() {
        return divisor;
    }

    public void setDivisor(int divisor) {
        this.divisor = divisor;
    }

    public Divisao addDividendo(int dividendo){
        this.dividendo = dividendo;
        return this;
    }

    public Divisao addDivisor(int divisor) {
        this.divisor = divisor;
        return this;
    }
    
}