package com.pedrobispo.avaliacao.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * <h1>Representa um contribuinte no sistema.</h1>
 * <p>
 * Os nomes de referência dos atributos com os campos no banco de dados estão
 * entre '\' para que o postgres aceite os nomes em uppercase.
 *
 * @author Pedro Bispo
 */
@Entity
@Table(name = "\"CONTRIBUINTE\"")
public class Contribuinte implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "\"COD_CONTRIBUINTE\"")
    private int id;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "\"COD_RUA\"")
    private Rua rua;

    @NotEmpty(message = "Informe o nome do contribuinte.")
    @Column(name = "\"NOME\"")
    private String nome;

    @Column(name = "\"CPF\"")
    private String cpf;

    @Column(name = "\"NUMERO\"")
    private int numero;

    @Column(name = "\"QTDE_IMOVEL\"")
    private int quantidade_imoveis;

    /**
     * Variável para receber e validar o CPF informado no formulário.
     */
    @Transient
    @CPF(message = "CPF INVÁLIDO.")
    private String cpfValido;

    public int getId() {
        return id;
    }

    public Contribuinte addId(int id) {
        this.id = id;
        return this;
    }

    public Contribuinte addNome(String nome) {
        this.nome = nome;
        return this;
    }

    public Contribuinte addRua(Rua rua) {
        this.rua = rua;
        return this;
    }

    /**
     * Além de setar o valor da variável cpfValido, também seta o valor da variável
     * Cpf com o mesmo valor. A idéia aqui é que, ao setar o valor da variável
     * cpfValido, o dado informado seja validado e somente após essa validação, a
     * variável Cpf seja setada, uma vez que se o valor não passar na validação, o
     * objeto não será persistido.
     * <p>
     * A validação poderia ser feita diretamente na variável Cpf, no entanto, a
     * validação considera os pontos e o traço que compõe o CPF brasileiro, mas a
     * variável no banco de dados tem um limite de 11 caracteres, armazendo somente
     * os números.
     * <p>
     * Método para uso de interface fluente.
     * 
     * @param cpfValido CPF informado no formulário.
     * @return Esse próprio contribuinte.
     */
    public Contribuinte addCpfValido(String cpf){
        this.cpfValido = cpf;   //aqui o cpf vindo da tela é validado
        this.setCpf(this.cpfValido);    //aqui o valor validado é inserido na variável que será persistida no banco de dados.
        return this;
    }

    public Contribuinte addQuantidade_imoveis(int quantidade_imoveis) {
        this.quantidade_imoveis = quantidade_imoveis;
        return this;
    }

    /**
     * 
     * Antes de setar o valor do cpf, remove os caracters especiais que possam ter
     * vindo do formulário
     * <p>
     * Método para uso de interface fluente.
     * 
     * @param cpf valor a ser inserido no campo CPF.
     * @return esse próprio contribuinte.
     */
    public Contribuinte addCpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public Contribuinte addNumero(int numero) {
        this.numero = numero;
        return this;
    }

    public Rua getRua() {
        return rua;
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public int getNumero() {
        return numero;
    }

    public int getQuantidade_imoveis() {
        return quantidade_imoveis;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRua(Rua rua) {
        this.rua = rua;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * 
     * Antes de setar o valor do cpf, remove os caracters especiais que possam ter
     * vindo do formulário
     * 
     * @param cpf valor a ser inserido no campo CPF.
     */
    public void setCpf(String cpf) {
        this.cpf = cpf.replaceAll("[^a-zZ-Z1-9 ]", "");
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setQuantidade_imoveis(int quantidade_imoveis) {
        this.quantidade_imoveis = quantidade_imoveis;
    }
   
    public String getCpfTemp() {
        return cpfValido;
    }

    /**
     * Além de setar o valor da variável cpfValido, também seta o valor da variável Cpf com o mesmo valor.
     * A idéia aqui é que, ao setar o valor da variável cpfValido, o dado informado seja validado e somente
     * após essa validação, a variável Cpf seja setada, uma vez que se o valor não passar na validação,
     * o objeto não será persistido.
     * 
     * A validação poderia ser feita diretamente na variável Cpf, no entanto, a validação considera os pontos
     * e o traço que compõe o CPF brasileiro, mas a variável no banco de dados tem um limite de 11 caracteres,
     * armazendo somente os números.
     * 
     * @param cpfValido CPF informado no formulário.
     */
    public void setCpfTemp(String cpfValido) {
        this.cpfValido = cpfValido;
        this.setCpf(this.cpfValido);
    }

    public String getCpfValido() {
        return cpfValido;
    }

    public void setCpfValido(String cpfValido) {
        this.cpfValido = cpfValido;
    }

}