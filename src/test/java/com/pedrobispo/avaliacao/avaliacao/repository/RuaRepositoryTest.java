package com.pedrobispo.avaliacao.avaliacao.repository;

import static org.assertj.core.api.Assertions.assertThat;
import com.pedrobispo.avaliacao.model.Rua;
import com.pedrobispo.avaliacao.repository.RuaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
* Teste unitário da classe Rua.
*
* Usa um banco de dados simulado em memória, para usar o banco de dados real,
* anotar com @AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
*
* @author  Pedro Bispo
*/
@RunWith(SpringRunner.class)
@DataJpaTest
public class RuaRepositoryTest{

    @Autowired
    public RuaRepository ruaRepository;

    /**
     * Gera uma rua com valores genéricos para os campos obrigatórios.
     * 
     * @return Contruinte com campos obrigatórios preenchidos.
     */
    private Rua getDefaultRua(){
        return new Rua().addNome(" ");
    }

    @Test
    public void testSaveRua(){

        Rua rua = getDefaultRua();
        this.ruaRepository.save(rua);

        assertThat(rua.getId()).isNotNull();
    }

    @Test
    public void testDeletRua(){

        Rua rua = getDefaultRua();
        this.ruaRepository.save(rua);
        assertThat(rua.getId()).isNotNull();

        ruaRepository.delete(rua);
        assertThat(ruaRepository.findOne(rua.getId())).isNull();
    }

    @Test
    public void testUpdateRua(){

        String primeiroNomeDaRua = "Rua de teste";
        String segundoNomeDaRua = "Rua testada";

        Rua rua = getDefaultRua().addNome(primeiroNomeDaRua);
        this.ruaRepository.save(rua);
        rua.setNome(segundoNomeDaRua);
        this.ruaRepository.save(rua);

        rua = ruaRepository.findOne(rua.getId());
        assertThat(rua.getNome()).isEqualTo(segundoNomeDaRua);
    }

}