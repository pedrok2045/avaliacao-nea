package com.pedrobispo.avaliacao.avaliacao.repository;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import com.pedrobispo.avaliacao.model.Contribuinte;
import com.pedrobispo.avaliacao.model.Rua;
import com.pedrobispo.avaliacao.repository.ContribuinteRepository;
import com.pedrobispo.avaliacao.repository.RuaRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
* Teste unitário da classe Contribuinte.
*
* Usa um banco de dados simulado em memória, para usar o banco de dados real,
* anotar com @AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
*
* @author  Pedro Bispo
*/
@RunWith(SpringRunner.class)
@DataJpaTest
public class ContribuinteRepositoryTest{

    @Autowired
    public ContribuinteRepository contribuinteRepository;

    @Autowired
    private RuaRepository ruaRepository;

    private String validCpf = "75629325817";

    /**
     * Gera um contribuinte com valores genéricos para os campos obrigatórios.
     * 
     * @return Contruinte com campos obrigatórios preenchidos.
     */
    private Contribuinte getDefaultContribuinte(){
        Rua rua = ruaRepository.save(new Rua().addNome(" "));
        return new Contribuinte().addNome(" ").addCpf(validCpf).addRua(rua);
    }

    @Test
    public void testSaveContribuinte(){

        Contribuinte contribuinte = getDefaultContribuinte();
        this.contribuinteRepository.save(contribuinte);

        assertThat(contribuinte.getId()).isNotNull();
    }

    @Test
    public void testDeleteContribuinte(){

        Contribuinte contribuinte = getDefaultContribuinte();
        this.contribuinteRepository.save(contribuinte);
        assertThat(contribuinte.getId()).isNotNull();

        contribuinteRepository.delete(contribuinte);
        assertThat(contribuinteRepository.findOne(contribuinte.getId())).isNull();
    }

    @Test
    public void testUpdateContribuinte(){

        String novoNome = "Maria";

        Contribuinte contribuinte = getDefaultContribuinte();
        this.contribuinteRepository.save(contribuinte);

        contribuinte.setNome(novoNome);
        this.contribuinteRepository.save(contribuinte);

        contribuinte = contribuinteRepository.findOne(contribuinte.getId());
        assertThat(contribuinte.getNome()).isEqualTo(novoNome);
    }



    @Test
    public void testFindByName(){

        String nome = "João";

        Contribuinte contribuinte = getDefaultContribuinte().addNome(nome);
        contribuinteRepository.save(contribuinte);

        Contribuinte contribuinteSalvo = contribuinteRepository.findByName(nome);

        assertThat(contribuinteSalvo.getNome()).isEqualTo(contribuinte.getNome());
    }

    @Test
    public void testNomeIniciaCom(){

        String primeiroNome = "Maria Josefa";
        String segundoNome = "José";
        String terceiroNome = "João";
        String termo = "J";
        int resultadosEsperados = 2;

        Contribuinte maria = getDefaultContribuinte().addNome(primeiroNome);
        contribuinteRepository.save(maria);

        Contribuinte jose = getDefaultContribuinte().addNome(segundoNome);
        contribuinteRepository.save(jose);

        Contribuinte joao = getDefaultContribuinte().addNome(terceiroNome);
        contribuinteRepository.save(joao);

        List<Contribuinte> contribuintes = contribuinteRepository.nomeIniciaCom(termo);
        assertThat(contribuintes.size()).isEqualTo(resultadosEsperados);
    }

    @Test
    public void testNomePossui(){

        String primeiroNome = "Maria da Silva Souza";
        String segundoNome = "José de Oliveira";
        String terceiroNome = "João Olavo de Carvalho";
        String termo = "Ol";
        int resultadosEsperados = 2;

        Contribuinte maria = getDefaultContribuinte().addNome(primeiroNome);
        contribuinteRepository.save(maria);

        Contribuinte jose = getDefaultContribuinte().addNome(segundoNome);
        contribuinteRepository.save(jose);

        Contribuinte joao = getDefaultContribuinte().addNome(terceiroNome);
        contribuinteRepository.save(joao);

        List<Contribuinte> contribuintes = contribuinteRepository.nomePossui(termo);
        assertThat(contribuintes.size()).isEqualTo(resultadosEsperados);
    }

    @Test
    public void testNomeTerminaCom(){

        String primeiroNome = "Maria da Silva Souza";
        String segundoNome = "José de Oliveira Souza";
        String terceiroNome = "João Souza Olavo de Carvalho";
        String termo = "Souza";
        int resultadosEsperados = 2;

        Contribuinte maria = getDefaultContribuinte().addNome(primeiroNome);
        contribuinteRepository.save(maria);

        Contribuinte jose = getDefaultContribuinte().addNome(segundoNome);
        contribuinteRepository.save(jose);

        Contribuinte joao = getDefaultContribuinte().addNome(terceiroNome);
        contribuinteRepository.save(joao);

        List<Contribuinte> contribuintes = contribuinteRepository.nomeTerminaCom(termo);
        assertThat(contribuintes.size()).isEqualTo(resultadosEsperados);
    }

    @Test
    public void testNomeIgualAh(){

        String primeiroNome = "Maria da Silva Souza";
        String segundoNome = "José de Oliveira Souza";
        String termo = "José de Oliveira Souza";
        int resultadosEsperados = 1;

        Contribuinte maria = getDefaultContribuinte().addNome(primeiroNome);
        contribuinteRepository.save(maria);

        Contribuinte jose = getDefaultContribuinte().addNome(segundoNome);
        contribuinteRepository.save(jose);

        List<Contribuinte> contribuintes = contribuinteRepository.nomeIgualAh(termo);
        assertThat(contribuintes.size()).isEqualTo(resultadosEsperados);
    }

}