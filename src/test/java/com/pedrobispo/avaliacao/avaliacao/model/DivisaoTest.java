package com.pedrobispo.avaliacao.avaliacao.model;

import static org.assertj.core.api.Assertions.assertThat;
import com.pedrobispo.avaliacao.model.Divisao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;


/**
* Teste da classe Divisão.
*
* @author  Pedro Bispo
*/
@RunWith(SpringRunner.class)
public class DivisaoTest {

    /**
     * Costantes representanto os números utilizados nos testes.
     */
    private final int ZERO = 0;
    private final int UM = 1;
    private final int DOIS = 2;
    private final int TRES = 3;
    private final int CINCO = 5;
    private final int SEIS = 6;
    private final int DEZ = 10;
    private final String INFINITO = "Infinito";

    /**
     * Objeto singleton da classe Divisão, para ser
     * usado nos testes.
     */
    private static Divisao divisaoSingleton;

    /**
     * Para não criar várias instâncias de Divisão,
     * uso do padrão singleton.
     * @return Instância singleton de Divisao.
     */
    private Divisao getDivisao() {
        if (divisaoSingleton == null)
            divisaoSingleton = new Divisao();
        return divisaoSingleton.reset();
    }

    /**
     * N / 0 = infinito
     */
    @Test
    public void testDividirPorZero(){

        /**
         * Essa estrutura foi utilizada somente para apreciação de como os testes poderiam
         * ser feitos usando interfaces fluentes.
         */
        assertThat(
            getDivisao().addDividendo(TRES).addDivisor(ZERO).dividir().getQuociente()
        )
        .isEqualToIgnoringCase(INFINITO);
    }

    /**
     * 0 / N = 0
     */
    @Test
    public void testDividirZero() {
        Divisao divisao = getDivisao().addDividendo(ZERO).addDivisor(TRES).dividir();

        assertThat(divisao.getQuociente()).isEqualTo(Integer.toString(ZERO));
    }

    /**
     * quando o resultado de a/b for menor que zero, deve retornar zero.
     */
    @Test
    public void testQuocienteMenorQueZero() {
        Divisao divisao = getDivisao().addDividendo(DOIS).addDivisor(TRES).dividir();

        assertThat(divisao.getQuociente()).isEqualTo(Integer.toString(ZERO));
    }


    @Test
    public void testDivisaoExata() {
        Divisao divisao = getDivisao().addDividendo(DEZ).addDivisor(DOIS).dividir();

        assertThat(divisao.getQuociente()).isEqualTo(Integer.toString(CINCO));
    }

    @Test
    public void testDivisaoNaoExata() {
        Divisao divisao = getDivisao().addDividendo(DEZ).addDivisor(TRES).dividir();

        assertThat(divisao.getQuociente()).isEqualTo(Integer.toString(TRES));
        assertThat(divisao.getResto()).isEqualTo(UM);
    }

    /**
     * positivo/negativo ou negativo/positivo resulta em negativo
     */
    @Test
    public void testDivisaoEntreNegativoEhPositivo() {
        Divisao primeiraDivisao = getDivisao().addDividendo(-DEZ).addDivisor(DOIS).dividir();
        assertThat(primeiraDivisao.getQuociente()).isEqualTo(Integer.toString(-CINCO));

        Divisao segundaDivisao = getDivisao().addDividendo(SEIS).addDivisor(-DOIS).dividir();
        assertThat(segundaDivisao.getQuociente()).isEqualTo(Integer.toString(-TRES));
    }

    /**
     * negativo/negativo resulta em positivo
     */
    @Test
    public void testDivisaoEntreNegativos() {
        Divisao divisao = getDivisao().addDividendo(-DEZ).addDivisor(-DOIS).dividir();

        assertThat(divisao.getQuociente()).isEqualTo(Integer.toString(CINCO));
    }
}